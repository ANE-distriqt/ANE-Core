

###### 2015.06.15

Removed debug code from AS lib
iOS: Updated to latest common lib
Android: Windows: Fix for bug in AIR packager resulting in missing resources
Android: x86 Support


###### 2015.06.09

Changed core symbol definitions


###### 2015.05.22

iOS: Corrected InvokeEvent operation (resolves #2, resolves distriqt/ANE-PushNotifications/#32, resolves distriqt/ANE-FacebookAPI/#27)


###### 2015.03.04

Initial release


